﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DraggingState {
	notDragging = 0,
	draggingView = 1,
	draggingObject = 2,
}

public class Dragger : MonoBehaviour {

	private Vector3 previousMousePosition;
	private GameObject dragee = null;

	private DraggingState state = DraggingState.notDragging;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButton ("Fire1") && state == DraggingState.notDragging) {
			var ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast (ray, out hit, 100)) {
				state = DraggingState.draggingObject;
				dragee = hit.collider.gameObject;
			} else {
				state = DraggingState.draggingView;
			}
		} else if (!Input.GetButton ("Fire1")) {
			state = DraggingState.notDragging;
		} else if (state == DraggingState.draggingObject) {
			dragee.transform.Translate (Camera.main.ScreenToWorldPoint(Input.mousePosition) - Camera.main.ScreenToWorldPoint(previousMousePosition));
		} else if (state == DraggingState.draggingView)
			Camera.main.transform.Translate (Camera.main.ScreenToWorldPoint(previousMousePosition) - Camera.main.ScreenToWorldPoint(Input.mousePosition));
		previousMousePosition = Input.mousePosition;
	}

}
