﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntVector2 {
	public readonly float x;
	public readonly int y;

	public IntVector2 (Vector3 originalVector) {
		x = originalVector.x;
		y = Mathf.RoundToInt(originalVector.y);
	}

	public IntVector2 (float x, int y) {
		this.x = x;
		this.y = y;
	}

	public override string ToString() {
		return string.Format ("({0}, {1})", x, y);
	}
}

public class RoadManager : MonoBehaviour {

    [SerializeField]
    private Sprite[] RoadLineSprites;

    private GameObject[] RoadLines;
	private SpriteRenderer[] RoadRenderers;

	public Camera camera;

	private int lineCount = Screen.height;

	public Vector3 [] positions = new Vector3[2];

    // Use this for initialization
    void Start () {
        RoadLines = new GameObject[lineCount];
		RoadRenderers = new SpriteRenderer[lineCount];

        for (int i = 0; i < lineCount; i++) {
            RoadLines[i] = new GameObject();
            RoadLines[i].transform.position = new Vector3(0, i, 0);
            var sloap = -1.0f / lineCount;
            var scale = sloap * i + 1;
            RoadLines[i].transform.localScale = new Vector3(scale, 1, 1);
			RoadRenderers[i] = RoadLines[i].AddComponent<SpriteRenderer>();
        }
	}
	
	// Update is called once per frame
	void Update () {
		// Clear road from previous frame
		foreach (var item in RoadRenderers) {
			item.sprite = null;
		}

		// Draw road
		for (int j = positions.Length - 1; j > 0; j--) {


			var screenPosition1 = camera.WorldToScreenPoint (positions [j - 1]);
			var screenPosition2 = camera.WorldToScreenPoint (positions [j]);

			// Not rendering backfaces
			if (screenPosition1.y < screenPosition2.y) {

				var screenPositionInt1 = new IntVector2 (screenPosition1);
				var screenPositionInt2 = new IntVector2 (screenPosition2);

				var distance1 = screenPosition1.z;
				var distance2 = screenPosition2.z;


				if (screenPositionInt1 == screenPositionInt2) {
					DrawRoadLine (screenPositionInt1, distance1, 0);
				} else {

					var step = 1f / (screenPositionInt2.y - screenPositionInt1.y);

					for (int i = 0; screenPositionInt1.y + i <= screenPositionInt2.y; i++) {
						var y = screenPositionInt1.y + i;
						var x = Mathf.Lerp (screenPosition1.x, screenPosition2.x, i * step);
						var distance = Mathf.Lerp (distance1, distance2, i * step);

						DrawRoadLine (new IntVector2 (x, y), distance, j % 2);
					}
				}
			}
		}
    }

	private void DrawRoadLine (IntVector2 linePosition, float distance, int spriteNumber) {
		if (linePosition.y >= 0 && linePosition.y < lineCount) {
			RoadLines[linePosition.y].transform.position = new Vector3 (linePosition.x , linePosition.y, 10);
			RoadLines[linePosition.y].transform.localScale = new Vector3 (-distance / 100 + 1, 1, 1);

			RoadRenderers[linePosition.y].sprite = RoadLineSprites [spriteNumber];
		}
	}
}
