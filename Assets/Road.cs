﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Road : MonoBehaviour {

	private const int kRoadLength = 500;

	public float curviness;
	public Vector3[] newVertices;
//	public Vector2[] newUV;
	public int[] newTriangles;


	void Start() 
	{
		Mesh mesh = new Mesh();
		GetComponent<MeshFilter>().mesh = mesh;

		newVertices = new Vector3[2 + kRoadLength * 2];
		newTriangles = new int[kRoadLength * 6];

		newVertices [0] = new Vector3 (-1, 0, 0);
		newVertices [1] = new Vector3 (1, 0, 0);

		float sinStep = 0;
		for (int i = 2; i < kRoadLength * 2 + 2; i += 2)
		{
			sinStep += 0.2f;
			newVertices [i] = new Vector3 (newVertices [0].x + Mathf.Sin(sinStep) * curviness, newVertices [i - 2].y, newVertices [i - 2].z + 1);
			newVertices [i + 1] = new Vector3 (newVertices [1].x + Mathf.Sin(sinStep) * curviness, newVertices [i - 1].y, newVertices [i - 1].z + 1);
		}

		for (int i = 0; i < kRoadLength * 2; i += 2)
		{
			var j = i * 3;
			newTriangles [j] = i;
			newTriangles [j + 1] = i + 2;
			newTriangles [j + 2] = i + 1;
			newTriangles [j + 3] = i + 1;
			newTriangles [j + 4] = i + 2;
			newTriangles [j + 5] = i + 3;
		}
			
			
		mesh.vertices = newVertices;
//		mesh.uv = newUV;
		mesh.triangles = newTriangles;

		GetComponent<MeshCollider> ().sharedMesh = mesh;
	}
}
