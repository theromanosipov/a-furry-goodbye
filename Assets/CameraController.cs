﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

	public float scrollSpeedMultiplier;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
//		transform.position = new Vector3 (
//			transform.position.x + Input.GetAxis ("Horizontal") * speedMultiplier,
//			transform.position.y + Input.GetAxis ("Vertical") * speedMultiplier,
//			transform.position.z);

		Camera.main.orthographicSize -= Input.GetAxis ("Mouse ScrollWheel") * scrollSpeedMultiplier;
		if (Camera.main.orthographicSize < 1)
			Camera.main.orthographicSize = 1;
	}
}
