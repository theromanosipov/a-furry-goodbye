﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class LevelEditor : MonoBehaviour {

	public GameObject nodePrefab;

	[SerializeField]
	private List<GameObject> RoadNodes = new List<GameObject>();

	private LineRenderer linerenderer;

	// Use this for initialization
	void Start () {
		linerenderer = GetComponent<LineRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
		// Update Line Renderer
		linerenderer.positionCount = RoadNodes.Count;
		linerenderer.SetPositions (RoadNodes.Select (n => n.transform.position).ToArray ());
	}

	public void AddNode() {
		GameObject newNode;
		if (RoadNodes.Count < 1) {
			newNode = Instantiate (nodePrefab, transform);
		} else {
			var previousNodePosition = RoadNodes [RoadNodes.Count - 1].transform.position;
			newNode = Instantiate (
				nodePrefab, 
				new Vector3(previousNodePosition.x + 5, previousNodePosition.y, previousNodePosition.z), 
				Quaternion.identity, 
				transform);
		}
		RoadNodes.Add (newNode);
	}

	[System.Serializable]
	private class SerializableRoad
	{
		public SerializableRoad(Vector3[] positions) 
		{
			this.positions = positions;
		}

		public Vector3[] positions;
	}

	public void SaveRoad() {
		SerializableRoad nodePositions = new SerializableRoad(RoadNodes.Select (n => n.transform.position).ToArray ());
		string json = JsonUtility.ToJson(nodePositions);
		Debug.Log (json);

	}
		
		
}
